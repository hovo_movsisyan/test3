﻿using System;

namespace ConsoleApp4
{
    class Program
    {
        

        static void Main(string[] args)
        {
            string n = "AnnA";
            Polindrom(n);
        }

        public static bool Polindrom(string x)
        {
            int k = x.Length / 2;
            int n = x.Length;
            for (int i = 0; i < k; i++)
            {
                if (x[i] != x[n - i - 1])
                {
                    return false;
                }
            }
            return true;
        }
        
    }
}
